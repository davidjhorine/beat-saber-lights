# Notice
## This repository is no longer updated
### This project is replaced by [LEDSaber](https://gitlab.com/davidjhorine/ledsaber/).

# Beat Saber Lights
### A tool to sync your smart lights with beat saber lighting data
## **Health Warning/User Agreement** 
**This program utilizes flashing lights! Those who are affected by flashing lights should not use this program or watch the preview video!**

By using this software, you assume all responsibility for any effects of it, including but not limited to:
* Device damage
* Health issues from flashing lights
* Unauthorized devices changes

Please also know that any files you add to the light-wrappers folder can contain possibly malicious code. Ensure you trust the source of your light-wrappers before using them.

This program is an unofficial program designed to work with custom songs designed for the video game Beat Saber. Neither I nor the files contained within this repository have any official connection to Beat Saber or Beat Games.

## Requirements
This program requires python3 to run. Download it from [here](https://www.python.org/downloads/) on Windows or Mac or from your package manager on Linux.

This program requires the the following python modules:
* os
* time
* sys
* importlib
* tkinter
* **json**
* **threading**
* **playsound**

The ones marked in bold are likely to not be installed unless you installed them yourself. Install them with `pip3 install [module name here]` (most Linuxes) or `pip install [module name here]` (most Windows systems).

The following python modules are only required for certain light-wrappers:
* flux_led (for Magic Home control via the flux_led_wrapper)
* openrgb-python (for OpenRGB device control)
* gpiozero (for Pi GPIO device control)

If you don't wish to install one of those modules, you can remove the .py file from the light-wrappers folder.

## Installation
Download the repository using the Gitlab website and unzip it. Alternatively, run:
`git clone https://gitlab.com/david-horine/beat-saber-lights.git`

## Usage
From the folder you cloned or downloaded in "installation", run 
`python3 beatsaber-lights.py` or `python beatsaber-lights.py` depending on your system. From there, you will shown the user agreement. A file dialog will appear. Navigate to the folder of the Beat Saber song you want to run and click the OK button. Follow the instructions to set the lights for each channel, your colors, and finally, your delay. Your song will then begin to play.

### Note about delay

Different delays work better depending on your computer's speed and the lights you are using. I have found a delay of about 3-3.5 works best for most situations.

### Advanced usage
To avoid the file dialog, a custom song can be specified using command line arguments. For example:
`python3 beatsaber-lights.py "C:/Program Files (x86)/Steam/steamapps/common/Beat Saber/Beat Saber_data/CustomLevels/1e9a (Example Song)/"`

### Programs (Early Beta and Likely to Break!)
Programs allow you to save a list of settings so you can quickly start Beat Saber Lights without going through the setup process each time. 

To create a program, run "create-program.py" and follow setup as normal. As with the main beatsaber-lights program, you can avoid the file dialog by passing a custom song folder as an argument.

To use a program, pass the path to the program file as an argument to beatsaber-lights. For example:
`python3 beatsaber-lights.py "/path/to/program/example program.bsl"`

## Example videos
### Example 1: [Parasite Eve](https://beatsaver.com/beatmap/b6b4) on an NZXT Hue+  
[![Beat Saber Lights Example 1](https://img.youtube.com/vi/bEAUMBbVVVo/0.jpg)](https://www.youtube.com/watch?v=bEAUMBbVVVo "Beat Saber Lights Example 1")\  
[Alternative link if the above video does not show](https://www.youtube.com/watch?v=bEAUMBbVVVo)  

### Example 2: [Cheat Codes](https://bsaber.com/songs/6620/) on an Ikea LED Strip  
[![Beat Saber Lights Example 2](https://img.youtube.com/vi/N1ENXuf3yuI/0.jpg)](https://www.youtube.com/watch?v=N1ENXuf3yuI "Beat Saber Lights Example 2")\  
[Alternative link if the above video does not show](https://www.youtube.com/watch?v=N1ENXuf3yuI)  

## Supported devices
Currently: 
* Any device that supports [OpenRGB](https://gitlab.com/CalcProgrammer1/OpenRGB)
* Any device that uses the Magic Home/Magic Home Pro app for control
* Any light that can be toggled using a Raspberry Pi GPIO

Here's what I've tested:
* NZXT Hue+ running through OpenRGB - works amazing for songs with slower lightmapping, has trouble keeping up with faster lightmapping
* Older, Hi-Flying based Magic Home LED bulb - works alright, but tends to hang behind heavily on commands. It will freeze and drop connections if too many commands are sent quickly
* Newer, ESP based Magic Home LED bulb - works much better, but flash effects are lessened by the firmware trying to smoothly fade between colors. Will factory reset itself if too many commands are sent quickly, but it's easy enough to set back up using the Magic Home app
* IKEA Dioder controlled using 2 relays and a Pi - works amazingly, it can keep up with fast lightmapping and is very responsive

## Adding devices
This program is made to be expandable to any type of light controllable with Python. Lights are added from small python programs called "wrappers" that exist in the light-wrappers folder. To add a wrapper, simply create a python program in that folder with the following functions:

### setup():

The setup function should return a list of dicts, each containing two keys, name and identifier. For example:

[{"name":"Example light bulb 1", "identifier":"192.168.1.12"},{"name":"Example light bulb 2", "identifier":"192.168.1.54"}]

would be a valid list to return. Name is what will be shown to the user when asking to choose what lights go to what channels, and identifier will be passed back to the module when an event happens.

### off(identifier):

The off function should immedietly turn the light off. The identifier value provided during setup will be returned.

### on(identifier,color)

The on function should immedietly turn on the light to the provided color. Color will be provided as a list of three RGB values 0-255.

### flash(identifier, color)

The flash function should turn on the light to a color slightly brighter than the provided color then change it to the provided color. In my wrappers, I do this by adding 20 to each RGB value for the flash color. You are, however, free to do this however you see fit for your light platform.

### fade_black(identifier, color)

The fade_black function should fade from the provided color value to black over the course of about half a second.

### Additional notes about adding new lights

Any setup that requries the user's attention should be done in the setup() function. This includes pressing a pairing button on a light or manually providing an IP for lights that do not have a scan function.

The print_wrapper provided will allow you to create virtual lights that print out what commands are sent to them. Simply change the number_of_print_lights variable at the top of the file. Alternatively, this file can be used as a guide for functions.
