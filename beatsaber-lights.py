import json #for loading the beat saber info and map files
import time #for timing the whole thing
import sys #for including light-wrappers in the path
from playsound import playsound #for playing the song
import os #for directory listing for the light-wrappers folder
import importlib #for importing from light-wrappers
import threading #for running the light commands in the background to preserve timing
from tkinter import filedialog, Tk #for showing the folder selection dialog
import dill #for loading program data

"""
Relevant lights:
Back Lasers: Event 0
Ring Lights: Event 1
Left Rotating Lasers: Event 2
Right Rotating Lasers: Event 3
Center Lights: Event 4

Relevant Values:
0: Turn light group off
1/5: Change Light to Blue/Red and stay on
2/6: Change Light to Blue/Red and flash brightly
3/7: Change Light to Blue/Red and fade to black
"""

print("BEAT SABER LIGHTS (version 2)")
print(
    """
    **This program utilizes flashing lights! Those who are affected by flashing lights should not use this program or watch the preview video!**
    
    By using this software, you assume all responsibility for any effects of it, including but not limited to:
    - Device damage
    - Health issues from flashing lights
    - Unauthorized devices changes

    Please also know that any files you add to the light-wrappers folder can contain possibly malicious code. Ensure you trust the source of your light-wrappers before using them.
    """
)

if sys.argv[0].endswith('py'): # If the first argument is the name of the python file
    args = sys.argv[1:] # Set args to the 1st argument onward
else:
    args = sys.argv # Set args to the 0th argument onward

if args == []: # If no argument is specified
    root = Tk() # Select the default tkinter window
    root.withdraw() # Get rid of the default window
    folder = filedialog.askdirectory() # Ask for a directory


folder = ' '.join(args) # Join together all the arguments passed to the program to account for situations where directory names may have a space
if folder.endswith('.bsl'): # The file extension "bsl" will be used to store programs
    program = True # This variable will be used to tell the program to load setup stuff from the bsl file
else:
    program = False # Will tell the program to ask all the user settings and stuff
    if not (folder.endswith('\\') or folder.endswith('/')): # Check if the folder ends with a \ or / and add / if it doesn't
        folder = folder + "/"

## READ LEVEL DATA TO VARIABLES ##
def read_data():
    global info
    global events
    try:
        with open(folder + 'info.dat') as json_file: # Import the song data
            info = json.load(json_file)
    except: # OH LOOK INFO.DAT IS CASE INSENSITIVE ISN'T THAT LOVELY (:
        with open(folder + 'Info.dat') as json_file: # Import the song data
            info = json.load(json_file)

    level = info['_difficultyBeatmapSets'][0]['_difficultyBeatmaps'][0]['_beatmapFilename'] # Get name of level file

    with open(folder + level) as json_file: # Import the level data
        levelstring = json.load(json_file)

    beatsPerSecond = info['_beatsPerMinute'] / 60 # Calculate the beats per second

    events = [] # Initialize the events variable
    for e in levelstring["_events"]: # For every event
        events.append({"second" : e['_time'] / beatsPerSecond , "event" : e['_type'] , "value" : e['_value']}) # Calculate the second it needs to fire at and append it to a list

    # Uncomment this section for debugging information related to calculations
    """
    print("Beats Per second: " + str(beatsPerSecond)) # Print beats per second
    for e in events: # For every event
        print(str(e['event']) + "," + str(e['value']) + " at " + str(e['second']) + " seconds") # Print event, value at second for debugging purposes
    """

## IMPORT LIGHT MODULES ##
def get_light_modules(opt):
    files = os.listdir('light-wrappers') # Get all files in 'light-wrappers' directory
    global lights # Make lights global since we have to use it pretty much everywhere
    lights = [] # Create an empty list of lights
    sys.path.append('light-wrappers') # Add the light wrappers folder to the path
    for f in files: # for every file
        if f.endswith('.py'): # that ends in .py
            module_to_import = f[:-3] # trim off the .py  
            module = importlib.import_module(module_to_import) # import the module
            new_lights = module.setup() # run setup and save the result to a list
            if not opt == "noset": # Allow passing "noset" to avoid adding lights to new list
                for light in new_lights: # for every light in the new lights
                    lights.append({"name":light['name'],"identifier":light['identifier'],"source":module}) # create a list entry with the name, identifier, and source

## LIST ALL LIGHTS ##
def print_lights():
    i = 0 # set the iteration variable
    for e in lights: # for every light
        print(str(i) + ": " + e['name'] + " from " + str(e['source']))
        i += 1

## GET LIGHTS FOR EACH CHANNEL ##
def get_light_settings():
    # Yes, I know setting a bunch of variables global is "lazy" and "bad practice". If it works and it's readable, I do not care.
    global back_lasers
    global ring_lights
    global center_lasers
    global left_lasers
    global right_lasers
    print("Enter a list of lights to attach to each channel, seperated by the / character (For example: 0/2/4 would attach lights 0, 2, and 4). Press Enter without entering any data to assign no lights to that channel.")
    back_lasers = input("Back lasers: ").split('/') # Get user input and store it as an array split by the / character vvv
    if type(back_lasers) != list: #If it isn't a list
        back_lasers = [back_lasers] #make it a list
    if "" in back_lasers: # If the list is just an empty string
        back_lasers = [] # Just set it to empty
    ring_lights = input("Ring lights: ").split('/')
    if type(ring_lights) != list: #If it isn't a list
        ring_lights = [ring_lights] #make it a list
    if "" in ring_lights: # If the list is just an empty string
        ring_lights = [] # Just set it to empty
    left_lasers = input("Left lasers: ").split('/')
    if type(left_lasers) != list: #If it isn't a list
        left_lasers = [left_lasers] #make it a list
    if "" in left_lasers: # If the list is just an empty string
        left_lasers = [] # Just set it to empty
    right_lasers = input("Right lasers: ").split('/')
    if type(right_lasers) != list: #If it isn't a list
        right_lasers = [right_lasers] #make it a list
    if "" in right_lasers: # If the list is just an empty string
        right_lasers = [] # Just set it to empty
    center_lasers = input("Center lights: ").split('/')
    if type(center_lasers) != list: #If it isn't a list
        center_lasers = [center_lasers] #make it a list
    if "" in center_lasers: # If the list is just an empty string
        center_lasers = [] # Just set it to empty

## GET COLORS ##
def get_colors():
    # Again with the global thing, this is changing old code, not writing new code
    global color1
    global color2
    print("Now decide your colors. These should be written as 3 numbers 0-255 seperated by the / character. For example: 255/0/0 would give you red and 0/0/255 would give you blue")
    color1 = input("First color, this is normally blue in beat saber: ").split('/')
    color2 = input("Second color, this is normally red in beat saber: ").split('/')
    color1[0] = int(color1[0]) # Fix colors by changing them from strings to ints
    color1[1] = int(color1[1])
    color1[2] = int(color1[2])
    color2[0] = int(color2[0])
    color2[1] = int(color2[1])
    color2[2] = int(color2[2])

## GET OFFSET ##
def get_offset():
    global offset
    offset = input("How many seconds would you like to offset your events by? Positive numbers make light events happen later, negative numbers make them happen earlier. ")

## SET UP FUNCTION FOR TURNING EVENT NUMBERS INTO LIGHTS
def evaluate_light_data(event,value):
    if event == 0: # If back lasers
        for light in back_lasers: # For every back laser
            value_eval(light, value) # Send light to value_eval
    elif event == 1:
        for light in ring_lights:
            value_eval(light, value)
    elif event == 2:
        for light in left_lasers:
            value_eval(light, value)
    elif event == 3:
        for light in right_lasers:
            value_eval(light, value)
    elif event == 4:
        for light in center_lasers:
            value_eval(light, value)

## TURN LIGHT AND VALUE INTO ACTION ##
def value_eval(light, value):
        if "0" in str(value): # Turn off lights
            # Start a new thread from the source (gotten from lights list) and identifier
            threading.Thread(target=lights[int(light)]['source'].off, args=[lights[int(light)]['identifier']]).start()
        elif "1" in str(value): # Lights on color one
            threading.Thread(target=lights[int(light)]['source'].on, args=(lights[int(light)]['identifier'],color1)).start()
        elif "2" in str(value): # Flash lights color 1
            threading.Thread(target=lights[int(light)]['source'].flash, args=(lights[int(light)]['identifier'],color1)).start()
        elif "3" in str(value): # Fade color 1 to black
            threading.Thread(target=lights[int(light)]['source'].fade_black, args=(lights[int(light)]['identifier'],color1)).start()
        elif "5" in str(value): # Lights on color 2
            threading.Thread(target=lights[int(light)]['source'].on, args=(lights[int(light)]['identifier'],color2)).start()
        elif "6" in str(value): # Flash color 2
            threading.Thread(target=lights[int(light)]['source'].flash, args=(lights[int(light)]['identifier'],color2)).start()
        elif "7" in str(value): # Fade color 2 to black
            threading.Thread(target=lights[int(light)]['source'].fade_black, args=(lights[int(light)]['identifier'],color2)).start()

## TURN OFF ALL THE LIGHTS THAT ARE ABOUT TO BE USED FOR EFFECTS ##
def all_off():
    for list in [back_lasers, ring_lights, left_lasers, right_lasers, center_lasers]: # For every list of lights
        for light in list: # For every light in that list
            lights[int(light)]['source'].off(lights[int(light)]['identifier']) # Get source and send off function with identifier


def run_timer():
  start = time.time() # Start the timer
  # Start a thread for playing the song audio based off the songFilename from the info file
  threading.Thread(target=playsound, args=[folder + info['_songFilename']]).start()
  step = 0 # Set current step
  while step < len(events): # While current step is less than total events
      if time.time() - (start + float(offset)) >= events[step]['second']: # If the current time is @ or greater than the time of the event
          #print("Fired event " + str(events[step]['event']) + "/" + str(events[step]['second']) + " at " + str(time.time() - start))
          #threading.Thread(target=evaluate_light_data, args=(events[step]['event'], events[step]['value']))
          evaluate_light_data(events[step]['event'],events[step]['value'])
          step += 1 # Increment the step

if __name__ == "__main__": # Run only if executed directly
    if program == True:
        get_light_modules("noset") # Get light modules, but don't write list to lights
        with open (folder, "rb") as program_file:
            program = dill.load(program_file)
        lights = program["lights"] # Load all the variables
        color1 = program["color1"]
        color2 = program["color2"]
        offset = program["offset"]
        folder = program["folder"]
        back_lasers = program["light_settings"]["back"]
        ring_lights = program["light_settings"]["ring"]
        left_lasers = program["light_settings"]["left"]
        right_lasers = program["light_settings"]["right"]
        center_lasers = program["light_settings"]["center"]
        read_data()
        all_off() # Actually start doing a thing
        run_timer()
    else:
        # Run chain to start program
        read_data()
        get_light_modules("none")
        print_lights()
        get_light_settings()
        get_offset()
        get_colors()
        all_off()
        run_timer()