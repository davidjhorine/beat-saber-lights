import json #for loading and saving program data
import sys #for including light-wrappers in the path
import os #for directory listing for the light-wrappers folder
import importlib #for importing from light-wrappers
from tkinter import filedialog, Tk #for showing the folder selection dialog
import dill #for saving the program, dill can save modules where pickle cannot

print("BEAT SABER LIGHTS PROGRAM CREATOR - Make sure you read the README for instructions")
print(
    """
    By using this software, you assume all responsibility for any effects of it, including but not limited to:
    - Device damage
    - Health issues from flashing lights
    - Unauthorized devices changes

    Please also know that any files you add to the light-wrappers folder can contain possibly malicious code. Ensure you trust the source of your light-wrappers before using them.
    """
)

if sys.argv[0].endswith('py'): # If the first argument is the name of the python file
    args = sys.argv[1:] # Set args to the 1st argument onward
else:
    args = sys.argv # Set args to the 0th argument onward

if args == []: # If no argument is specified
    root = Tk() # Select the default tkinter window
    root.withdraw() # Get rid of the default window
    folder = filedialog.askdirectory() # Ask for a directory

folder = ' '.join(args) # Join together all the arguments passed to the program to account for situations where directory names may have a space
if not (folder.endswith('\\') or folder.endswith('/')): # Check if the folder ends with a \ or / and add / if it doesn't
    folder = folder + "/"

## IMPORT LIGHT MODULES ##
def get_light_modules(opt):
    files = os.listdir('light-wrappers') # Get all files in 'light-wrappers' directory
    global lights # Make lights global since we have to use it pretty much everywhere
    lights = [] # Create an empty list of lights
    sys.path.append('light-wrappers') # Add the light wrappers folder to the path
    for f in files: # for every file
        if f.endswith('.py'): # that ends in .py
            module_to_import = f[:-3] # trim off the .py  
            module = importlib.import_module(module_to_import) # import the module
            new_lights = module.setup() # run setup and save the result to a list
            if not opt == "noset": # Allow passing "noset" to avoid adding lights to new list
                for light in new_lights: # for every light in the new lights
                    lights.append({"name":light['name'],"identifier":light['identifier'],"source":module}) # create a list entry with the name, identifier, and source

## LIST ALL LIGHTS ##
def print_lights():
    i = 0 # set the iteration variable
    for e in lights: # for every light
        print(str(i) + ": " + e['name'] + " from " + str(e['source']))
        i += 1

## GET LIGHTS FOR EACH CHANNEL ##
def get_light_settings():
    # Yes, I know setting a bunch of variables global is "lazy" and "bad practice". If it works and it's readable, I do not care.
    global back_lasers
    global ring_lights
    global center_lasers
    global left_lasers
    global right_lasers
    print("Enter a list of lights to attach to each channel, seperated by the / character (For example: 0/2/4 would attach lights 0, 2, and 4). Press Enter without entering any data to assign no lights to that channel.")
    back_lasers = input("Back lasers: ").split('/') # Get user input and store it as an array split by the / character vvv
    if type(back_lasers) != list: #If it isn't a list
        back_lasers = [back_lasers] #make it a list
    if "" in back_lasers: # If the list is just an empty string
        back_lasers = [] # Just set it to empty
    ring_lights = input("Ring lights: ").split('/')
    if type(ring_lights) != list: #If it isn't a list
        ring_lights = [ring_lights] #make it a list
    if "" in ring_lights: # If the list is just an empty string
        ring_lights = [] # Just set it to empty
    left_lasers = input("Left lasers: ").split('/')
    if type(left_lasers) != list: #If it isn't a list
        left_lasers = [left_lasers] #make it a list
    if "" in left_lasers: # If the list is just an empty string
        left_lasers = [] # Just set it to empty
    right_lasers = input("Right lasers: ").split('/')
    if type(right_lasers) != list: #If it isn't a list
        right_lasers = [right_lasers] #make it a list
    if "" in right_lasers: # If the list is just an empty string
        right_lasers = [] # Just set it to empty
    center_lasers = input("Center lights: ").split('/')
    if type(center_lasers) != list: #If it isn't a list
        center_lasers = [center_lasers] #make it a list
    if "" in center_lasers: # If the list is just an empty string
        center_lasers = [] # Just set it to empty

## GET COLORS ##
def get_colors():
    # Again with the global thing, this is changing old code, not writing new code
    global color1
    global color2
    print("Now decide your colors. These should be written as 3 numbers 0-255 seperated by the / character. For example: 255/0/0 would give you red and 0/0/255 would give you blue")
    color1 = input("First color, this is normally blue in beat saber: ").split('/')
    color2 = input("Second color, this is normally red in beat saber: ").split('/')
    color1[0] = int(color1[0]) # Fix colors by changing them from strings to ints
    color1[1] = int(color1[1])
    color1[2] = int(color1[2])
    color2[0] = int(color2[0])
    color2[1] = int(color2[1])
    color2[2] = int(color2[2])

## GET OFFSET ##
def get_offset():
    global offset
    offset = input("How many seconds would you like to offset your events by? Positive numbers make light events happen later, negative numbers make them happen earlier. ")

if __name__ == "__main__":
    get_light_modules("none")
    print_lights()
    get_light_settings()
    get_colors()
    get_offset()
    export = {
        "lights" : lights,
        "color1" : color1,
        "color2" : color2,
        "offset" : offset,
        "folder" : folder,
        "light_settings" : {
            "back" : back_lasers,
            "ring" : ring_lights,
            "left" : left_lasers,
            "right" : right_lasers,
            "center" : center_lasers,
        }
    }
    filename = input("What should this program be called? ")
    with open(filename + '.bsl', "wb") as output:
        dill.dump(export, output)