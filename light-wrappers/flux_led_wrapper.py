import flux_led

def setup():
    scanner = flux_led.BulbScanner()
    bulb_obj = []
    bulbs = scanner.scan(timeout=5)
    i = 0
    for e in bulbs:
        # print ("Light " + str(i) + ": " + e['id'] + " @ " + e['ipaddr'])
        bulb_obj.append({'name':e['id'] + ' @ ' + e['ipaddr'],'identifier':e['ipaddr']})
        i += 1
    return bulb_obj
        
def off(identifier):
    flux_led.WifiLedBulb(identifier).setRgb(0,0,0, persist=False)

def on(identifier,color):
    flux_led.WifiLedBulb(identifier).setRgb(color[0], color[1], color[2], persist=False)

def flash(identifier,color):
    def add_fix(num):
        num += 20
        if num > 255:
            num = 255
        return num
    flash_color = []
    flash_color.append(add_fix(color[0]))
    flash_color.append(add_fix(color[1]))
    flash_color.append(add_fix(color[2]))
    flux_led.WifiLedBulb(identifier).setRgb(flash_color[0], flash_color[1], flash_color[2], persist=False)
    flux_led.WifiLedBulb(identifier).setRgb(color[0], color[1], color[2], persist=False)

def fade_black(identifier,color):
    flux_led.WifiLedBulb(identifier).setRgb(color[0], color[1], color[2], persist=False)
    while color[0] + color[1] + color[2] > 0:
        if color[0] > 0:
            color[0] -= 1
        if color[1] > 0:
            color[1] -= 1
        if color[2] > 0:
            color[2] -= 1
        flux_led.WifiLedBulb(identifier).setRgb(color[0], color[1], color[2], persist=False)
    flux_led.WifiLedBulb(identifier).setRgb(0, 0, 0, persist=False)