from openrgb import OpenRGBClient
from openrgb.utils import RGBColor
from time import sleep
try:
    client = OpenRGBClient()
    devices = client.devices
except:
    print("Failed to connect to OpenRGB. Do you have the server running?")
    devices = []

def setup():
    lights = []
    e = 0
    for device in devices:
        if hasattr(device, 'zones'):
            i = 0
            for zone in device.zones:
                zone = zone
                lights.append(
                    {
                        "name" : device.name + ": Zone " + str(i),
                        "identifier": str(e) + "|" + str(i)
                    }
                )
                i += 1
        else:
            lights.append(
                {
                    "name" : device.name,
                    "identifier" : str(i)
                }
            )
        e += 1
    return lights

def get_device(identifier):
    if "|" in identifier:
        identifier = identifier.split('|')
        device = devices[int(identifier[0])].zones[int(identifier[1])]
    else:
        device = devices[int(identifier)]
    return device


def off(identifier):
    device = get_device(identifier)
    device.set_color(RGBColor(0,0,0))

def on(identifier, color):
    device = get_device(identifier)
    device.set_color(RGBColor(*color))

def flash(identifier, color):
    device = get_device(identifier)
    def add_fix(num):
        num += 20
        if num > 255:
            num = 255
        return num
    flash_color = []
    flash_color.append(add_fix(color[0]))
    flash_color.append(add_fix(color[1]))
    flash_color.append(add_fix(color[2]))
    device.set_color(RGBColor(*flash_color))
    sleep(.1)
    device.set_color(RGBColor(*color))

def fade_black(identifier,color):
    device = get_device(identifier)
    device.set_color(RGBColor(*color))
    while color[0] + color[1] + color[2] > 0:
        if color[0] > 0:
            color[0] -= 20
            if color[0] < 0:
                color[0] = 0
        if color[1] > 0:
            color[1] -= 20
            if color[1] < 0:
                color[1] = 0
        if color[2] > 0:
            color[2] -= 20
            if color[2] < 0:
                color[2] = 0
        device.set_color(RGBColor(*color))
    device.set_color(RGBColor(0,0,0))
