import time

# This wrapper is disabled by default; change this to "True" to enable it
enable_me = False

# Configure pins here. If you do this remotely, you will likely have to preceed the command with PIGPIO_ADDR=<ip> GPIOZERO_PIN_FACTORY=pigpio
red_pin = 23
blue_pin = 18

# Here's where things actually start
def setup():
    if enable_me:
        import gpiozero
        global blue
        global red
        blue = gpiozero.LED(blue_pin) # Creates objects for two pins
        red = gpiozero.LED(red_pin)
        return [
            {"name":"Raspberry Pi GPIO Light", "identifier":"none"} # This wrapper only supports one light, identifiers are irrelevant
        ]
    else:
        return []

def off(identifier):
    red.off()
    blue.off()

def on(identifier,color):
    if color[0] > color[2]:
        red.on()
        blue.off()
    else:
        red.off()
        blue.on()

def flash(identifier,color):
    if color[0] > color[2]:
        red.on()
        blue.on()
        time.sleep(.05)
        blue.off()
    else:
        red.on()
        blue.on()
        time.sleep(.05)
        red.off()

def fade_black(identifier,color):
    if color[0] > color[2]:
        blue.off()
        red.on()
        time.sleep(.5)
        red.off()
    else:
        blue.on()
        red.off()
        time.sleep(.5)
        blue.off()
