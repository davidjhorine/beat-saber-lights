number_of_print_lights = 0 # number of virtual print lights to create


def setup(): # Setup is called every time a module is imported. Scanning for lights and returning a list should go here.
    lights = [] # create an empty lights list
    print("Setup called") # print 'setup called' for example purposes. Ideally, your light wrapper should not print anything
    i = 0 # Set up the iteration variable
    while i < number_of_print_lights: # while iteration variable is less than the defined number of print lights
        lights.append( # Appends a new light to the list
            { # Lights are represented by dicts with 2 values
                "name": "Print Light" + str(i), # The name value is what is shown to the user in the UI. It should be human readable.
                "identifier": i # The identifier value is passed back to your module whenever an action is called
            }
        )
        i += 1 # iterate the iteration variable
    return lights # Make sure to return your list of lights!

def off(identifier):
    print("Light " + str(identifier) + " turned off")

def on(identifier,color):
    print("Light " + str(identifier) + " is on to " + str(color))

def flash(identifier,color):
    print("Light " + str(identifier) + " flashed to " + str(color))

def fade_black(identifier,color):
    print("Light " + str(identifier) + " went from " + str(color) + " to black.")